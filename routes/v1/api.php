<?php
declare(strict_types=1);


use App\Http\Controllers\Admin\IndexController;
use App\Http\Controllers\Admin\PostController;
use Illuminate\Support\Facades\Route;

//modified in RouteServiceProvider
Route::get('/admin', function () {
    return view('/admin/index');
})->name("admin");



Route::get('pages/index',IndexController::class);


require __DIR__ . '/../auth.php';


