<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\VotesController;
use App\Models\Category;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');


Route::middleware('auth')->group(function () {

    Route::get('/home', function () {
        return view('home');
    })->name('home');

    Route::get('/about-us', function () {
        return view('about');
    })->name('about-us');

    //Contacts
    Route::get('/contact-us', function () {
        return view('contacts');
    })->name('contact-us');


//Posts
    Route::post('/contact-us', [ContactController::class, 'store'])->name('contact.us.store');

    Route::get('/blog-page', [PostController::class, 'index'])->name('blog-page');

    Route::resource('/posts', PostController::class);

    //Categories
    Route::get('/categories/{category:slug}', function (Category $category) {
        return view('categories.index', ['category' => $category]);
    });

    //Votes
    Route::post('/votes/{post}/like', [VotesController::class, 'likes'])->name('likes');
    Route::post('/votes/{post}/dislike', [VotesController::class, 'dislikes'])->name('dislikes');

    //Comments
    Route::get('/comment/index/{slug}', [CommentController::class, 'index'])->name('comment.index');
    Route::post('/comment/store', [CommentController::class, 'store'])->name('comment.add');


    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
