<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;


/**
 * @extends Factory
 */
class CommentFactory extends Factory
{



    public function definition()
    {
        $content = $this->faker->text();
        return [
            'post_id' => Post::factory(),
            'user_id' => User::factory(),
            'content'=> $content,

        ];
    }
}
