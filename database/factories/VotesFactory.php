<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;


/**
 * @extends Factory
 */
class VotesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition():?Array
    {


        return [
            'user_id'       => User::factory(),
            'post_id'       => Post::factory(),
            "liked" => $this->faker->boolean(),

        ];
    }



}
