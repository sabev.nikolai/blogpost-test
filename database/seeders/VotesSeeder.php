<?php

namespace Database\Seeders;
use App\Models\Votes;
use Illuminate\Database\Seeder;

class VotesSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run():void
    {
        Votes::factory()->count(50)->create();
    }
}
