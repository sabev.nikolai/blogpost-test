<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreContactRequest;
use App\Services\ContactService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;


class ContactController extends Controller
{

    public function store(StoreContactRequest $request, ContactService $contact) : RedirectResponse
    {

        $contact->CreateContact($request);
        return Redirect::back()->with(['success' => 'Thank you for contacting us. We will stay in touch.Good bye!']);
    }
}
