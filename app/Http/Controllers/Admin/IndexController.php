<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;
use App\Http\Resources\Admin\JsonAdminResource;
use Symfony\Component\HttpFoundation\JsonResponse;

class IndexController
{
    public function __invoke(): JsonResponse
    {
        return new JsonResponse(
            data: new JsonAdminResource(
            [],
        ));


    }
}
