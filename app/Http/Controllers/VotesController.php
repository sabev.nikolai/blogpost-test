<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;

class VotesController extends Controller
{

    public function likes(Post $post, Request $request):RedirectResponse
    {
        $user = User::where('id', '=', $request->user()->id)->first();
        $post->like($user);
        return Redirect::back()->with('status', 'Liked' );

    }
    public function dislikes(Post $post, Request $request):RedirectResponse
    {
        $user = User::where('id', '=', $request->user()->id)->first();
        $post->dislike($user);
        return Redirect::back()->with('status', 'Disliked' );
    }
}
