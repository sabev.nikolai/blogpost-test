<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;


class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     */

    public function index($id): View

    {
        $posts = Post::with(['comments'])->where('id', $id)->get();

        $post = [];
        foreach ($posts as $array) {
            $post = $array;
        }


        return view('comments.index', compact('post'));

    }

    public function store(Request $request): RedirectResponse
    {
        $comment = new Comment;
        $comment->content = $request->get('comment_body');
        $comment->user()->associate($request->user());
        $post = Post::find($request->get('post_id'));
        $post->comments()->save($comment);

        return Redirect::back()->with('status', 'profile-updated');


    }
}
