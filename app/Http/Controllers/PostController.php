<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Models\Post;
use App\Repository\Interfaces\PostRepositoryInterface;
use App\Repository\PostRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class PostController extends Controller
{

    /**
     * @var mixed|PostRepositoryInterface
     */

    private mixed $postRepository;

    /**
     * PostController constructor.
     * @param PostRepository $postRepository
     */

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function index(): View
    {
        $posts = $this->postRepository->index();
        return view('posts.index', compact('posts'));
    }

    /**
     * @return View
     */
    public function create(): View
    {
        $categories = $this->postRepository->create();
        return view('posts.create', compact('categories'));
    }

    public function store(StorePostRequest $request): RedirectResponse
    {
        $this->postRepository->store($request);
        return redirect()->route('posts.index')->with('status', 'Post Created Successfully');
    }

    /**
     * @param Post $post
     * @return View
     */

    public function show(Post $post): View
    {
        $like = $this->postRepository->show($post);
        return view('posts.show', compact('post', 'like'));
    }

    /**
     * Verify user to edit own post
     * @param Post $post
     * @return object
     */
    public function edit(Post $post): object
    {
        if (Auth::user()->getAuthIdentifier() === $post->user_id) {
            $categories = $this->postRepository->edit();
            return view('posts.edit', compact('categories', 'post'));
        }
        return Redirect::route('posts.index')->with('error', 'Sorry,not allowed to edit others posts.');

    }

    /**
     *
     * @param StorePostRequest $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(StorePostRequest $request, $id): RedirectResponse
    {
        $this->postRepository->update($request, $id);
        return Redirect::route('posts.index')->with('status', 'Post Updated Successfully');
    }

    /**
     * Verify user to delete own posts only
     * @param Post $post

     */
    public function destroy(Post $post): RedirectResponse
    {
        if (Auth::user()->getAuthIdentifier() === $post->user_id) {
            return Redirect::back()->with('status', 'Post deleted successfully');
        }
        return Redirect::route('posts.index')->with('error', 'Sorry,not allowed to delete others posts');
    }

}
