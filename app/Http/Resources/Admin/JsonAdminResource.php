<?php

declare(strict_types=1);
namespace App\Http\Resources\Admin;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

 class JsonAdminResource extends JsonResource
{


     /**
      * Transform the resource into an array.
      * @param Request $request
      * @return array
      */

    public function toArray($request):array
    {
       $userList = User::all()->where('role',"admin");
       return [$userList];
    }
}
