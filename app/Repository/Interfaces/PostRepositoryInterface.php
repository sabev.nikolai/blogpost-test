<?php
declare(strict_types=1);
namespace App\Repository\Interfaces;

interface PostRepositoryInterface
{
    public function index();
    public function create();
    public function store($request);
    public function show($post);
    public function edit();
    public function update($data, $id);
    public function destroy($post);


}
