<?php

declare(strict_types=1);
namespace App\Repository;

use App\Models\Category;
use App\Models\Post;
use App\Models\Votes;
use App\Repository\Interfaces\PostRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class PostRepository implements PostRepositoryInterface
{

    public function index(): object
    {
        return Post::with('likes')->withCount(['likes', 'dislikes', 'comments'])
            ->orderBy('id', 'desc')
            ->paginate(5);

    }

    public function create(): Collection
    {
        return Category::all();
    }

    public function store($request): Post
    {
        return Post::create([
            'user_id' => Auth::user()->getAuthIdentifier(),
            'title' => $request->title,
            'slug' => Str::slug($request->slug),
            'description' => $request->description,
            'category_id' => $request->category_id,
        ]);
    }

    public function show($post)
    {

        return Votes::where(
            ['user_id' => Auth::getUser()->getAuthIdentifier(),
                'post_id' => $post->id]
        )->first();


    }

    public function edit()
    {
        return Category::all();
    }

    public function update($data, $id)
    {
        $post = Post::where('id', $id)->first();
        $post->title = $data['title'];
        $post->slug = $data['slug'];
        $post->description = $data['description'];
        $post->category_id = $data->category_id;
        $post->save();
    }

    public function destroy($post)
    {
        $post->delete();
    }


}
