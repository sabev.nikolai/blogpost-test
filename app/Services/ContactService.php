<?php

declare(strict_types=1);
namespace App\Services;


use App\Models\Contact;
use Illuminate\Http\Request;

class ContactService {
    public function CreateContact(Request $request): Contact
    {
        return Contact::create([
          'name' => $request->name,
          'email'=> $request->email,
          'phone'=> $request->phone,
          'subject' => $request->subject,
          'message' => $request->message,

      ]);
    }
}
