<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Post extends Model
{
    use HasFactory;


    protected $fillable = [
        'user_id',
        'title',
        'slug',
        'description',
        'category_id',
        'post_id',
        'liked'

    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function votes(): HasMany
    {
        return $this->hasMany(Votes::class);
    }

    /**
     * The has Many Relationship
     *
     * @var array
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }


    public function like($user, $liked = true): object
    {
        return $this->votes()->updateOrCreate(['user_id' => $user->id,], ['liked' => $liked]);
    }


    public function dislike($user): string
    {
        return $this->like($user, false);
    }


    //potentional functionality to show most hated posts
    public function dislikes(): HasMany
    {
        return $this->hasMany(Votes::class)->where('liked', false);
    }


    public function likes(): HasMany
    {
        return $this->hasMany(Votes::class)->where('liked', true);
    }


}
