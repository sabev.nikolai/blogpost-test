<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Votes extends Model
{
    protected $fillable = [
        'user_id','post_id','liked'
    ];
    use HasFactory;

    public function post():BelongsTo{
        return $this->belongsTo('PostRepository');
    }



}
