<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;

use Illuminate\Mail\Mailable;

use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public mixed $data;

    /**
     * Create a new message instance.
     */

    public function __construct(mixed $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     */


    public function build():Mailable
    {
        return $this->subject('Contact US - '. $this->data->subject)
            ->view('emails.contact');
    }
}
