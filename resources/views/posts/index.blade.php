<x-app-layout>
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            {{ __('Blog Posts') }}
        </h2>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </x-slot>

    <div class="py-12">
        <div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
            <div class="overflow-hidden bg-white shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div x-data="{ showMessage: true }" x-show="showMessage" class="flex justify-center">
                        @if (session()->has('status'))
                            <div
                                class="flex items-center justify-between max-w-xl p-4 bg-white border rounded-md shadow-sm">
                                <div class="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8 text-green-500"
                                         viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd"
                                              d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                                              clip-rule="evenodd"/>
                                    </svg>
                                    <p class="ml-3 text-sm font-bold text-green-600">{{ session()->get('status') }}</p>
                                </div>
                                <span @click="showMessage = false" class="inline-flex items-center cursor-pointer">
                                <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 text-gray-600" fill="none"
                                     viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M6 18L18 6M6 6l12 12"/>
                                </svg>
                            </span>
                            </div>
                        @elseif (session()->has('error'))
                            <p class="ml-3 text-sm font-bold text-red-600">{{ session()->get('error') }}</p>
                        @endif
                    </div>

                    <div class="mt-1 mb-4 flex-right">
                        <a class="px-2 py-2 text-sm text-white bg-blue-600 rounded"
                           href="{{ route('posts.create') }}">{{ __('Add New Post') }}</a>
                    </div>
                    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                            <thead
                                class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="px-6 py-3">
                                    #
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Title
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Category
                                </th>
                                <th scope="col" class="px-6 py-3 "  >
                                    Votes Up
                                </th>

                                <th scope="col" class="px-6 py-3">
                                    Votes Down
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    <div>Commented#</div>
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Edit
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Show
                                </th>

                                <th scope="col" class="px-6 py-3">
                                    Delete
                                </th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($posts as $post)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <th scope="row"
                                        class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                                        {{$post->id}}
                                    </th>
                                    <td class="px-6 py-4">
                                        {{$post->title}}

                                    </td>
                                    <td class="px-6 py-4">
                                        <a href="/categories/{{$post->category->slug}}">{{$post->category->name}}</a>

                                    </td>
                                    <td class="px-6 py-4">
                                    <p>{{$post["likes_count"]}}</p>
                                    </td>

                                    <td class="px-6 py-4">
                                        <p>{{$post["dislikes_count"]}}</p>
                                    </td>
                                    <td class="px-6 py-4 text-danger underline ">
                                        <a href="/comment/index/{{$post->id}}">{{$post->comments->count()}}</a>
                                    </td>
                                    <td class="px-6 py-4">
                                        <a href="{{ route('posts.edit',$post->id) }}">Edit</a>
                                    </td>
                                    <td class="px-6 py-4">
                                        <a href="{{ route('posts.show',$post->id) }}">Show</a>
                                    </td>
                                    <td class="px-6 py-4">
                                        <form action="{{ route('posts.destroy',$post->id) }}" method="POST"
                                              onsubmit="return confirm('{{ trans('are You Sure ? ') }}');"
                                              style="display: inline-block;">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">

                                            <input type="submit" class="px-4 py-2 text-white rounded"
                                                   value="Delete">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>

                    </div>
                    <div class="py-10 pl-14 m-1">
                        {{$posts->links()}}
                    </div>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
