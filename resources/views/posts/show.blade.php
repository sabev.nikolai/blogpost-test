<x-app-layout>
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            {{ __('Blog posts') }}
        </h2>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </x-slot>

    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400 ">
            <thead
                class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="px-6 py-3">
                    Post#
                </th>
                <th scope="col" class="px-6 py-3">
                    Title
                </th>

                <th scope="col" class="px-6 py-3">
                    Content
                </th>

                <th scope="col" class="px-6 py-3">
                    <p class="text-red ">Vote Now</p>
                </th>

            </tr>
            </thead>
            <tbody>

            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                <th scope="row"
                    class="px-6 py-3 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                {{$post->id}}

                <td class="px-6 py-3 ">
                    {{$post->title}}
                </td>

                <td class="px-6 py-3">
                    <p class=" align-baseline ">{{$post->description}}</p>

                </td>

                <td class="px-9 py-3 pl-5">
                    @if($like == null  )
                        <form action="{{ route('likes', $post) }}" method="POST"
                              onsubmit="return confirm('{{ trans('Thank you! ') }}');"
                              style="display: inline-block;">
                            @csrf
                            <button type="submit">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="red"
                                     class="w-5 h-5">
                                    <path
                                        d="M1 8.25a1.25 1.25 0 112.5 0v7.5a1.25 1.25 0 11-2.5 0v-7.5zM11 3V1.7c0-.268.14-.526.395-.607A2 2 0 0114 3c0 .995-.182 1.948-.514 2.826-.204.54.166 1.174.744 1.174h2.52c1.243 0 2.261 1.01 2.146 2.247a23.864 23.864 0 01-1.341 5.974C17.153 16.323 16.072 17 14.9 17h-3.192a3 3 0 01-1.341-.317l-2.734-1.366A3 3 0 006.292 15H5V8h.963c.685 0 1.258-.483 1.612-1.068a4.011 4.011 0 012.166-1.73c.432-.143.853-.386 1.011-.814.16-.432.248-.9.248-1.388z"/>
                                </svg>
                            </button>

                        </form>

                        <form action="{{ route('dislikes', $post->id) }}" method="POST"
                              onsubmit="return confirm('{{ trans('Thank you! ') }}');"
                              style="display: inline-block;">

                            @csrf
                            <button type="submit">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="blue"
                                     class="w-5 h-5">
                                    <path
                                        d="M18.905 12.75a1.25 1.25 0 01-2.5 0v-7.5a1.25 1.25 0 112.5 0v7.5zM8.905 17v1.3c0 .268-.14.526-.395.607A2 2 0 015.905 17c0-.995.182-1.948.514-2.826.204-.54-.166-1.174-.744-1.174h-2.52c-1.242 0-2.26-1.01-2.146-2.247.193-2.08.652-4.082 1.341-5.974C2.752 3.678 3.833 3 5.005 3h3.192a3 3 0 011.342.317l2.733 1.366A3 3 0 0013.613 5h1.292v7h-.963c-.684 0-1.258.482-1.612 1.068a4.012 4.012 0 01-2.165 1.73c-.433.143-.854.386-1.012.814-.16.432-.248.9-.248 1.388z"/>
                                </svg>
                            </button>

                        </form>
                    @endif

                    @if($like)
                        <p class="px-1 py-3 font-medium text-red-500 dark:text-white whitespace-nowrap">{{ $like->liked ? 'Liked' : 'Not liked' }}</p>
                    @endif
                </td>
            </tr>
            </tbody>
        </table>

        <div class="max-w-2xl mx-auto p-4 sm:p-6 lg:p-8">
            <form method="post" action="{{ route('comment.add') }}">

                @csrf

                <div class="form-group">
                    <input type="text" name="comment_body" class="form-control"/>
                    <input type="hidden" name="post_id" value="{{ $post->id }}"/>
                </div>

                <button
                    class="bg-white hover:bg-red-500 text-gray-800 text-tahiti mt-3 py-1 px-4 border border-gray-400 rounded shadow">
                    {{ __('Send') }}
                </button>
                <button
                    class="bg-white  hover:bg-[#1da1f2] text-gray-800 text-tahiti mt-3 py-1  px-4 border border-gray-400 rounded shadow">
                    <a href="{{ route('posts.index') }}" class="btn btn-default">Back</a>
                </button>
            </form>
        </div>
    </div>
</x-app-layout>
