<x-app-layout>
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            {{ __('Blog Comments') }}
        </h2>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
        {{--    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">--}}
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </x-slot>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <hr/>
                        <h4>Current Comments</h4>
                        @if($post->comments->count() > 0)
                            @foreach($post->comments as $comments)
                                <br class="display-comment">
                                <p class="text-comment">"{{ $comments->content }}"</p>
                                <hr/>
                                <p>By {{ $comments->user->name }} on </p>
                                <p class="text-danger ">
                                    {{  date("d M Y", strtotime($comments->created_at)) }}
                                </p>

                    </div>
                    @endforeach
                    @endif
                    @if(empty($post->comments->count()))
                        <p class="text-danger">No comments left. Leave first comment now?</p>
                        <hr/>
                        <form method="post" action="{{ route('comment.add') }}">

                            @csrf

                            <div class="form-group">
                                <input type="text" name="comment_body" class="form-control"/>
                                <input type="hidden" name="post_id" value="{{ $post->id }}"/>
                            </div>


                            <button
                                class="bg-white  hover:bg-red-500 text-gray-800 text-tahiti mt-3 py-1 px-4 border border-gray-400 rounded shadow">
                                {{ __('Send') }}
                            </button>
                            <button
                                class="bg-white hover:bg-[#1da1f2] text-tahiti mt-3 py-1  px-4 border border-gray-400 rounded shadow">
                                <a href="{{ route('posts.index') }}" class="btn btn-default">Back</a>
                            </button>

                        </form>

                    @endif

                    @if (Session::has('success'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert">
                                <i class="fa fa-times"></i>
                            </button>
                            <strong>Success !</strong> {{ session('success') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
</x-app-layout>
