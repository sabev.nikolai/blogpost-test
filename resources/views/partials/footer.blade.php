<footer class="pt-5 my-5">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
    Created by <a href="https://www.linkedin.com/in/nikolay-sabev-24a6196a/">Nikolai Sabev</a> &middot; &copy; {{ date('Y') }}
</footer>
