<x-app-layout>
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            {{ __('Categories Page') }}
        </h2>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </x-slot>

    <div class="py-12">
        <div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
            <div class="overflow-hidden bg-white shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div x-data="{ showMessage: true }" x-show="showMessage" class="flex justify-center">
                        @if (session()->has('status'))
                            <div
                                class="flex items-center justify-between max-w-xl p-4 bg-white border rounded-md shadow-sm">
                                <div class="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-8 h-8 text-green-500"
                                         viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd"
                                              d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                                              clip-rule="evenodd"/>
                                    </svg>
                                    <p class="ml-3 text-sm font-bold text-green-600">{{ session()->get('status') }}</p>
                                </div>
                                <span @click="showMessage = false" class="inline-flex items-center cursor-pointer">
                                <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 text-gray-600" fill="none"
                                     viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M6 18L18 6M6 6l12 12"/>
                                </svg>
                            </span>
                            </div>
                        @endif
                    </div>

                    <div class="row">

                    </div>
                    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                            <thead
                                class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="px-6 py-3">
                                    #
                                </th>
                                <th scope="col" class="px-6 py-3">
                                  Post Title
                                </th>
                                <th scope="col" class="px-6 py-3">
                                    Post #
                                </th>

                                <th scope="col" class="px-6 py-3">
                                     Category
                                </th>

                                <th scope="col" class="px-6 py-3">
                                    Read
                                </th>
                            </tr>
                            </thead>
                            <tbody>


                               @foreach ($category->post as $post)

                                   <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                       <th scope="row"
                                           class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                                           {{$category->id}}
                                       </th>
                                       <th scope="row"
                                           class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                                           {{$post->title}}
                                       </th>

                                       <th scope="row"
                                           class="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                                           {{$post->id}}
                                       </th>

                                       <td class="px-6 py-4">

                                           {{$category->name}}
                                       </td>

                                       <td class="px-6 py-4">
                                           <a href="{{ route('posts.show',$post->id) }}">Read</a>
                                       </td>

                                       <td class="px-6 py-4">


                                       </td>
                                   </tr>
                               @endforeach
                            </tbody>

                        </table>

                    </div>

                </div>

            </div>
        </div>
    </div>
</x-app-layout>
